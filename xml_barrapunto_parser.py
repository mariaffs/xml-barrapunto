#!/usr/bin/python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
import urllib.request

print("<html><body>")

class ContentHandler(ContentHandler):

    def __init__ (self):
        self.inItem = False
        self.inContent = False
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.inContent = True

    def endElement (self, name):
        if name == 'item':
            self.inItem = False
        elif self.inItem:
            if name == 'title':
                self.title = self.theContent
                print('<p>Title: ' + self.title + '.<br></p>')
                self.inContent = False
                self.theContent = ""
            elif name == 'link':
                self.link = self.theContent
                print('<p>Link: ' + '<a href='+ self.link + '>'+ self.title + '</a></p>')
                self.inContent = False
                self.theContent = ""
                self.title = ""
                self.link = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog

# Load parser and driver

MyParser = make_parser()
MyHandler = ContentHandler()
MyParser.setContentHandler(MyHandler)

# Ready, set, go!

xmlFile = urllib.request.urlopen("http://barrapunto.com/index.rss")
MyParser.parse(xmlFile)

print('</body></html>')
print ("Parse complete")

